<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth', 'prefix' => ''], function() {
    Route::get('chat', 'ChatsController@index');
    Route::get('messages', 'ChatsController@fetchMessages');
    Route::post('messages', 'ChatsController@sendMessage');
    Route::get('private-messages/{from_id}', 'ChatsController@privateMessages');
    Route::post('private-messages/{user}', 'ChatsController@sendPrivateMessage');
    Route::get('user-chat', 'ChatsController@userChat');
    Route::post('search/user', 'ChatsController@searchUser');
    /**
     * Delete Conversation
     */
    Route::post('/updateSettings', 'ChatsController@updateSettings')->name('avatar.update');
    /**
     * Set active status
     */
    Route::post('/setActiveStatus', 'ChatsController@setActiveStatus')->name('activeStatus.set');
});
