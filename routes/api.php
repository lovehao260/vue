<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('uploadAvatar/{id}', 'API\UserController@uploadAvatar');
Route::get('user-chat/{id}', 'API\UserController@userChat');
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('details', 'API\UserController@details');

});
