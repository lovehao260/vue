<?php
namespace App\Http\Controllers\API;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Image;
use Validator;
class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * login api
     *
     * @return Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $success,'user' => $user,
                ], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
    /**
     * Register api
     *
     * @return Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus);
    }
    /**
     * details api
     *
     * @return Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus);
    }

    public function uploadAvatar(Request $request){
        $user = User::find($request->id);

        if(Storage::disk('public')->exists( config('minhhao.user_avatar.folder'),$user->avatar)) {
            File::delete('storage/users-avatar/'.$user->avatar);
        }
        $image = $request->avatar;  // your base64 encoded
        $name=(string) Str::uuid(). "." .'jpg';
        $user->avatar=$name;
        $user->save();
//        Storage::disk('public')->put($imageName, base64_decode($image));
        \Image::make($image)->save(public_path('storage/users-avatar/').$name);

        return response()->json(User::find($request->id) ,200);

    }
    public function userChat(Request $request){
        // get all users that received/sent message from/to [Auth user]
        $users = Message::join('users',  function ($join) {
            $join->on('messages.user_id', '=', 'users.id')
                ->orOn('messages.from_id', '=', 'users.id');

        })
            ->where('messages.user_id',$request->id)
            ->orWhere('messages.from_id', $request->id)
            ->select('users.id','users.name','users.avatar','messages.message','messages.created_at')
            ->orderBy('messages.created_at', 'desc')
            ->get();


        return array_unique($users);

    }

}
