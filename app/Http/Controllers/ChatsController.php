<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\User;

use Illuminate\Http\Request;
use App\Message;
use Illuminate\Support\Facades\Auth;
use App\Events\MessageSent;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Intervention\Image\Image;
use App\Events\PrivateMessageSent;
use Illuminate\Support\Facades\DB;
class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('chat');
    }

    public function fetchMessages()
    {
        return Message::with('user')->get();
    }

    public function sendMessage(Request $request)
    {
        $user = Auth::user();
        $message = $user->messages()->create([
            'message' => $request->input('message')
        ]);
//        broadcast(new MessageSent($user, $message))->toOthers();
        return ['status' => $user];
    }
    public function privateMessages(Request $request){
            $conversation_id = Conversation::where([['user_id','=',Auth::id()],['from_id','=', $request->from_id]])->first();
            $conversation_2 = Conversation::where([['user_id','=',$request->from_id],['from_id','=',Auth::id() ]])->first();
            $message =DB::table('messages')->join('conversation', 'messages.conversation_id', '=', 'conversation.id')
                ->select('messages.*','conversation.from_id','conversation.user_id')
                ->where('conversation_id',$conversation_2['id'])
                ->orWhere('conversation_id',$conversation_id['id'])
                ->orderBy('messages.created_at', 'ASC')->get()
                ->toArray();
            DB::table('messages')->join('conversation', 'messages.conversation_id', '=', 'conversation.id')
            ->where('conversation_id',$conversation_2['id'])
            ->orWhere('conversation_id',$conversation_id['id'])
            ->update(['read' => true]);

            return $message;
    }
    public function sendPrivateMessage(Request $request )
    {
        $user = Auth::user();
        $name='';
        $conversation = new Conversation();
        $check = Conversation::where([['user_id','=',$user->id],['from_id','=', $request->from_id]])->count();
        if ($check == 0){
            $conversation->user_id =$user->id;
            $conversation->from_id =$request->from_id;
            $conversation->save();
        }

        if($request->get('file_name'))
        {
            $image = $request->get('file_name');
            $name = Str::uuid() .'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('file_name'))->save(public_path('storage/attachments/').$name);
        }
        $conversation_id = Conversation::where([['user_id','=',$user->id],['from_id','=', $request->from_id]])->first();
        $message = $conversation->messages()->create([
            'conversation_id'=>$conversation_id->id,
            'message' => $request->messages,
            'attachment' => $name,
        ]);

        broadcast(new MessageSent($message,$request->from_id));

        return response()->json(['state' => 1, 'data' => $message]);
    }
    public function searchUser(Request $request)
    {
        $search = $request->name;
        return  $dataName = User::where('name', 'LIKE', "%{$search}%")
//            ->orWhere('email', 'LIKE', '%' . $search . '%')
            ->paginate(10);

    }

    public function userChat(){
        // get all users that received/sent message from/to [Auth user]

        $contacts = DB::table('users')->join('conversation',  function ($join) {
            $join->on('users.id', '=', 'conversation.from_id');
            $join->orOn('users.id', '=', 'conversation.user_id');
        })
            ->where('conversation.user_id',Auth::id())
            ->orWhere('conversation.from_id',Auth::id())
            ->select('users.name','users.id','users.avatar')
            ->groupby('id')
            ->get();
        $unreadIds =DB::table('messages')->join('conversation', 'messages.conversation_id', '=', 'conversation.id')
            ->select(\DB::raw('`user_id` as sender_id, count(`user_id`) as messages_count'))
            ->where('from_id',Auth::id())
            ->where('read', false)
            ->groupBy('user_id')
            ->get();
        // add an unread key to each contact with the count of unread messages
        $contacts = $contacts->map(function($contact) use ($unreadIds) {
            $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();
            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;
            return $contact;
        });


        return (['users' =>$contacts,'user_id'=>Auth::id()]);
    }

    /**
     * Set user's active status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setActiveStatus(Request $request)
    {
        $update = $request['status'] > 0
            ? User::where('id', $request['user_id'])->update(['active_status' => 1])
            : User::where('id', $request['user_id'])->update(['active_status' => 0]);
        // send the response
        return Response::json([
            'status' => $update,
        ], 200);
    }

    public function updateSettings(Request $request){
        $msg = null;
        $error = $success = 0;
        $validator = Validator::make($request->all(), [
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
        ]);

        if (!$validator->fails()) {
            // if there is a [file]
            if ($request->hasFile('avatar')) {
                $file = $request->file('avatar');
                $file_old=User::find( Auth::user()->id);
                if(Storage::disk('public')->exists( config('minhhao.user_avatar.folder'),$file_old->avatar)) {
                    File::delete('storage/users-avatar/'.$file_old->avatar);
                }
                // upload
                $avatar = Str::uuid() . "." . $file->getClientOriginalExtension();
                $update = User::where('id', Auth::user()->id)->update(['avatar' => $avatar]);
                $file->storeAs("public/" . config('minhhao.user_avatar.folder'), $avatar);
               return $update ? 1 : 0;
            }
        } else {
            $error = "File extension not allowed!";
            return 1;
        }
        // send the response
        return Response::json([
            'status' => $success ? 1 : 0,
            'error' => $error ? 1 : 0,
            'message' => $error ? $msg : 0,
        ], 200);
    }

}
