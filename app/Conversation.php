<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Conversation extends Model
{
    protected $table = 'conversation';
    protected $fillable = ['user_id','from_id','status'];

    /**
     * A message belong to a user
     *
     * @return BelongsTo
     */
    public function messages()
    {
        return $this->belongsTo(Message::class);
    }
}
