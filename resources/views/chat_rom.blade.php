<html class="fontawesome-i2svg-active fontawesome-i2svg-complete">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="route" content="user">
    <meta name="csrf-token" content="vBoYWSfpVOlqsn6RHCOmxtA0MAzG45LibG615oBE">
    <meta name="url" content="http://localhost/chat/public/chatify" data-user="1">
    <script src="{{asset('js/font.awesome.min.js')}}"></script>
    <script src="{{asset('js/autosize.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/nprogress@0.2.0/nprogress.css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/light.mode.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">

</head>
<body>
<div class="messenger">

    <div class="messenger-infoView app-scroll">

        <nav>
            <a href="#">
                <svg class="svg-inline--fa fa-times fa-w-11" aria-hidden="true" focusable="false" data-prefix="fas"
                     data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"
                     data-fa-i2svg="">
                    <path fill="currentColor"
                          d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
                </svg><!-- <i class="fas fa-times"></i> --></a>
        </nav>
        <div class="avatar av-l"
             style="background-image: url(&quot;http://localhost/chat/public/storage/users-avatar/275bc3f9-60cc-41af-9479-59b02d161627.png&quot;);"></div>
        <p class="info-name">Le Minh Hao</p>
        <div class="messenger-infoView-btns">

            <a href="#" class="danger delete-conversation" style="display: inline;">
                <svg class="svg-inline--fa fa-trash-alt fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas"
                     data-icon="trash-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"
                     data-fa-i2svg="">
                    <path fill="currentColor"
                          d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path>
                </svg><!-- <i class="fas fa-trash-alt"></i> --> Delete Conversation</a>
        </div>

        <div class="messenger-infoView-shared" style="display: block;">
            <p class="messenger-title">shared photos</p>
            <div class="shared-photos-list"><p class="message-hint"><span>Nothing shared yet</span></p></div>
        </div>
    </div>
</div>

<div id="imageModalBox" class="imageModal">
    <span class="imageModal-close">×</span>
    <img class="imageModal-content" id="imageModalBoxSrc">
</div>


<div class="app-modal" data-name="delete">
    <div class="app-modal-container">
        <div class="app-modal-card" data-name="delete" data-modal="0">
            <div class="app-modal-header">Are you sure you want to delete this?</div>
            <div class="app-modal-body">You can not undo this action</div>
            <div class="app-modal-footer">
                <a href="javascript:void(0)" class="app-btn cancel">Cancel</a>
                <a href="javascript:void(0)" class="app-btn a-btn-danger delete">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="app-modal" data-name="alert">
    <div class="app-modal-container">
        <div class="app-modal-card" data-name="alert" data-modal="0">
            <div class="app-modal-header"></div>
            <div class="app-modal-body"></div>
            <div class="app-modal-footer">
                <a href="javascript:void(0)" class="app-btn cancel">Cancel</a>
            </div>
        </div>
    </div>
</div>

<div class="app-modal" data-name="settings">
    <div class="app-modal-container">
        <div class="app-modal-card" data-name="settings" data-modal="0">
            <form id="updateAvatar" action="http://localhost/chat/public/chatify/updateSettings"
                  enctype="multipart/form-data" method="POST">
                <input type="hidden" name="_token" value="vBoYWSfpVOlqsn6RHCOmxtA0MAzG45LibG615oBE">
                <div class="app-modal-header">Update your profile settings</div>
                <div class="app-modal-body">

                    <div class="avatar av-l upload-avatar-preview"
                         style="background-image: url('http://localhost/chat/public/storage/users-avatar/275bc3f9-60cc-41af-9479-59b02d161627.png');"></div>
                    <p class="upload-avatar-details"></p>
                    <label class="app-btn a-btn-primary update">
                        Upload profile photo
                        <input class="upload-avatar" accept="image/*" name="avatar" type="file" style="display: none">
                    </label>

                    <p class="divider"></p>
                    <p class="app-modal-header">Dark Mode
                        <svg class="svg-inline--fa fa-moon fa-w-16 dark-mode-switch" data-mode="0" aria-hidden="true"
                             focusable="false" data-prefix="far" data-icon="moon" role="img"
                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                            <path fill="currentColor"
                                  d="M279.135 512c78.756 0 150.982-35.804 198.844-94.775 28.27-34.831-2.558-85.722-46.249-77.401-82.348 15.683-158.272-47.268-158.272-130.792 0-48.424 26.06-92.292 67.434-115.836 38.745-22.05 28.999-80.788-15.022-88.919A257.936 257.936 0 0 0 279.135 0c-141.36 0-256 114.575-256 256 0 141.36 114.576 256 256 256zm0-464c12.985 0 25.689 1.201 38.016 3.478-54.76 31.163-91.693 90.042-91.693 157.554 0 113.848 103.641 199.2 215.252 177.944C402.574 433.964 344.366 464 279.135 464c-114.875 0-208-93.125-208-208s93.125-208 208-208z"></path>
                        </svg><!-- <span class="
                        far fa-moon dark-mode-switch" data-mode="0"></span> --></p>

                    <p class="divider"></p>
                    <p class="app-modal-header">Change Chatify Messenger Color</p>
                    <div class="update-messengerColor">
                        <a href="javascript:void(0)" class="messengerColor-1"></a>
                        <a href="javascript:void(0)" class="messengerColor-2"></a>
                        <a href="javascript:void(0)" class="messengerColor-3"></a>
                        <a href="javascript:void(0)" class="messengerColor-4"></a>
                        <a href="javascript:void(0)" class="messengerColor-5"></a>
                        <br>
                        <a href="javascript:void(0)" class="messengerColor-6"></a>
                        <a href="javascript:void(0)" class="messengerColor-7"></a>
                        <a href="javascript:void(0)" class="messengerColor-8"></a>
                        <a href="javascript:void(0)" class="messengerColor-9"></a>
                        <a href="javascript:void(0)" class="messengerColor-10"></a>
                    </div>
                </div>
                <div class="app-modal-footer">
                    <a href="javascript:void(0)" class="app-btn cancel">Cancel</a>
                    <input type="submit" class="app-btn a-btn-success update" value="Update">
                </div>
            </form>
        </div>
    </div>

</div>
<script>
    // Messenger global variable - 0 by default
    messenger = "0";
</script>
</body>
</html>
