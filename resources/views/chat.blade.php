@extends('layouts.app')

@section('content')
    <div class="messenger">
        @include('chat.letf_view')
        @include('chat.message_view')
        @include('chat.modals')
        <about-user></about-user>

    </div>
@endsection
{{--<div class="card-body msg_card_body">--}}
{{--    <chat-messages :messages="messages"></chat-messages>--}}
{{--</div>--}}
{{--<div class="card-footer">--}}
{{--    <div class="input-group">--}}
{{--        <chat-form--}}
{{--            v-on:messagesent="addMessage"--}}
{{--            :user="{{ Auth::user() }}"--}}
{{--        ></chat-form>--}}
{{--    </div>--}}
{{--</div>--}}
