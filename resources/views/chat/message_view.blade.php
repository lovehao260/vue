<div class="messenger-messagingView">
    <info-user :messages="messages"></info-user>
    <div class="internet-connection successBG-rgba" style="display: none;">
        <span class="ic-connected" style="display: inline;">Connected</span>
        <span class="ic-connecting" style="display: none;">Connecting...</span>
        <span class="ic-noInternet" style="display: none;">No internet access</span>
    </div>
    <chat-messages ></chat-messages>
    <chat-form></chat-form>

</div>

