/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
require('./bootstrap');
import 'fullcalendar/dist/fullcalendar.css';
window.Vue = require('vue');
import FullCalendar from 'vue-full-calendar'; //Import Full-calendar
Vue.use(FullCalendar);

Vue.component('example', require('./components/ExampleComponent.vue').default);
Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
Vue.component('chat-form', require('./components/ChatForm.vue').default);
Vue.component('search-form', require('./components/SearchUser.vue').default);
Vue.component('info-user', require('./components/userInfo.vue').default);
Vue.component('about-user', require('./components/AboutUser').default);
Vue.component('calendar-report', require('./components/userInfo.vue'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.Pusher = require('pusher-js');
import {store} from "./store/store";


Vue.config.productionTip = false;
Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).fromNow()
    }
});


const app = new Vue({
    el: '#app',
    store,
    data: {
        messages: []
    },
});
