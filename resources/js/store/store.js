import  Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        data_message:[],
        user_id: '',
        user_name: '',
        user_avatar: '',
    },

    // mutations: {
    //     increment (state) {
    //         state.count++
    //     }
    // }
});
